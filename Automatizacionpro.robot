*** Settings ***
Library   SeleniumLibrary

*** Variables ***
${browser}   chrome
${homepage}   http://automationpractice.com/index.php
${Selection}   Other

***Keywords***
Select Woman Option
    Click Element   xpath=//*[@id="block_top_menu"]/ul/li[1]/a
    Title Should Be   Woman - My Store
Select Dresses Option
    Click Element   xpath=//*[@id="block_top_menu"]/ul/li[2]/a
    Title Should Be   Dresses - My Store

*** Test Cases ***
001 Caso con Cndicionales
   Open Browser   ${homepage}   ${browser}
   Wait Until Element Is Visible   xpath=//*[@id="header_logo"]/a/img
   Run Keyword If   '${Selection}'=='Woman'   Select Woman Option   Else    Select Dresses Option
   Close Browser