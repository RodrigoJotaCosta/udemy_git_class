*** Settings ***
Library   SeleniumLibrary

*** Variables ***
${palabra a buscar}   rodrigoprueb@outlook.com
${navegador}   firefox

*** Test Cases ***
001 Abrir pagina y mandar mail con archivo adjunto
    Open Browser    https://outlook.live.com/owa/   ${navegador}
    Wait Until Element Is Visible  xpath=/html/body/header/div/aside/div/nav/ul/li[2]/a
    #Hace clic en iniciar sesion, escribe la direccion de correo, y apreta siguiente
    Click Element   xpath=/html/body/header/div/aside/div/nav/ul/li[2]/a
    Input Text    xpath=//*[@id="i0116"]   ${palabra a buscar}
    Click Element   xpath=//*[@id="idSIButton9"]
    #Espera por el campo de contrasena,la escribe y apreta siguiente
    Wait Until Element Is Visible   xpath=/html/body/div/form[1]/div/div/div[2]/div[1]/div/div/div/div/div/div[3]/div/div[2]/div/div[2]/div/div[2]/input
    Input Text   xpath=/html/body/div/form[1]/div/div/div[2]/div[1]/div/div/div/div/div/div[3]/div/div[2]/div/div[2]/div/div[2]/input   3lR0b0t()
    Click Element   xpath=//*[@id="idSIButton9"]
    #Espera por el boton mensaje nuevo y lo apreta
    Wait Until Element Is Visible   xpath=/html/body/div[2]/div/div[2]/div[2]/div[1]/div/div/div[1]/div[1]/div[2]/div/div/button/span   45s
    Click Element   xpath=/html/body/div[2]/div/div[2]/div[2]/div[1]/div/div/div[1]/div[1]/div[2]/div/div/button/span
    #Espera por boton Para y lo apreta
    Wait Until Element Is Visible   xpath=/html/body/div[2]/div/div[2]/div[2]/div[1]/div/div/div[3]/div[2]/div/div[3]/div[1]/div/div/div/div[1]/div[1]/div[1]/div/div[1]/div/div/div/div/div[1]/div/div/div   40s
    Click Element   xpath=/html/body/div[2]/div/div[2]/div[2]/div[1]/div/div/div[3]/div[2]/div/div[3]/div[1]/div/div/div/div[1]/div[1]/div[1]/div/div[1]/div/div/div/div/div[1]/div/div/div
    #Espera hasta que sean visibles los contactos de yoni y laureano y los selecciona y cliquea boton guardar
    Wait Until Element Is Visible   xpath=/html/body/div[6]/div/div/div/div[2]/div[2]/div/div[2]/div/section[2]/section/div[2]/section/div/div/div/div[1]/section/button[2]   40s
    Click Element   xpath=/html/body/div[6]/div/div/div/div[2]/div[2]/div/div[2]/div/section[2]/section/div[2]/section/div/div/div/div[1]/section/button[2]
    Click Element   xpath=/html/body/div[6]/div/div/div/div[2]/div[2]/div/div[2]/div/section[2]/section/div[2]/section/div/div/div/div[2]/section/button[2]/span
    Click Element   xpath=/html/body/div[6]/div/div/div/div[2]/div[2]/div/div[2]/div/div/button[1]/span
    #Espera por la barra de escribir asunto, la selecciona y escribe el asunto 
    Wait Until Element Is Visible   css=#TextField120   30s
    Input Text   css=#TextField120   Soy inmueble
    #Clic en adjuntar, esperar por el boton seleccionar las ubicaciones de la nube y seleccionarlo
    Click Element   xpath=/html/body/div[2]/div/div[2]/div[2]/div[1]/div/div/div[3]/div[1]/div/span/div/div/div/div/div[1]/div[3]/button/span/span
    Wait Until Element Is Visible   css=._3DK98xzwCp8B30u6WN3xB2   30s
    Click Element   css=._3DK98xzwCp8B30u6WN3xB2
    #Espero por cartel de pdf drive, lo selecciono y click en boton siguiente
    Wait Until Element Is Visible    xpath=/html/body/div[8]/div/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/div[2]/div/div/div/div[2]/div/div[2]/div/div/div/img   30s
    Click Element   xpath=/html/body/div[8]/div/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/div[2]/div/div/div/div[2]/div/div[2]/div/div/div/img
    Click Element   xpath=/html/body/div[8]/div/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div[3]/div/button/span/span
    #Espero que sea visible y selecciono el boton adjuntar como una copia
    Wait Until Element Is Visible   xpath=/html/body/div[10]/div/div/div/div[2]/div[2]/div/div[4]/button[2]   30s
    Click Element   xpath=/html/body/div[10]/div/div/div/div[2]/div[2]/div/div[4]/button[2]
    #Le mando el codigo para que espere a que el archivo se cargue y luego apuesto enviar
    Sleep   12s
    Click Element   xpath=/html/body/div[2]/div/div[2]/div[2]/div[1]/div/div/div[3]/div[2]/div/div[3]/div[1]/div/div/div/div[1]/div[3]/div[2]/div[1]/div/span/button[1]/span