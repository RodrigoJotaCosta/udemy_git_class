*** settings ***
documentation       Existe en un documento de texto los pasos manuales 
...                 Esta es mi primera automatizacion
Library             Selenium2Library


*** Variables ***
${palabra a buscar}   casos de prueba  
${navegador}   chrome
*** Test Cases ***
G001 Búsqueda de palabras en google
    Open Browser   https://www.google.com/  ${navegador}
    Wait Until Element Is Visible   xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input
    Input Text   xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input  ${palabra a buscar}
   # Click Element   xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[2]/div[2]/div[2]/center/input[1]
    Press Keys   None   RETURN
    Title Should Be   ${palabra a buscar} - Buscar con Google
    Page Should Contain   ${palabra a buscar}
    Close Browser

    
G002 Hacer click en el boton de google sin escribir palabras en Google
    Open Browser   https://www.google.com/  ${navegador}
    Wait Until Element Is Visible   xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input
    Click Element     xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input            
    Click Element     xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]
    Title Should Be    Google
    Close Browser