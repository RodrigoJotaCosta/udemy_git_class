*** Settings ***
Library   SeleniumLibrary
Resource  PaginadeWinston.robot

*** Test Cases ***
Winstonpage Casos de prueba
    Open homepage
    Title Should Be   Hola Mundo!
    Click Link   link=Este Link te dirije a otra ventana  
    Wait Until Element Is Visible   xpath=//*[@id="post-513"]/div[1]/a
    Title Should Be   Winston Castillo – Un sitio para comunicarse
    Close Browser
   
Abrir Ventana Modal
    [Tags]    TestError
    Open homepage
    Title Should Be   Hola Mundo!
    Set Focus To Element   xpath=/html/body/div[1]/div/div[2]/a[2]
    Click Link   link=Este hace aparecer una ventana nueva
    Title Should Be   Hola Mundo!
    Wait Until Element Is Visible   xpath=//*[@id="exampleModal"]/div/div/div[1]
    Close Browser